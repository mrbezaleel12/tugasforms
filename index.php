<?php
if(isset($_POST['submit'])) {
    $nama = $_POST['nama'];
    $mapel = $_POST['mapel'];
    $uts = $_POST['uts'];
    $uas = $_POST['uas'];
    $tgs = $_POST['tgs'];
    
    //Mencari nilai akhir
    $nilai_akhir = $uts * 0.35 + $uas * 0.5 + $tgs * 0.15;

    //Menampilkan Grade dari nilai akhir
    if ($nilai_akhir>=90) {
        $grade = "A";
    } elseif ($nilai_akhir>=70) {
        $grade = "B";
    } elseif ($nilai_akhir>=50) {
        $grade = "C";
    } else{
        $grade = "D";
    }
}
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Tugas PHP !</title>
  </head>

  <body style="background-color: rgb(140, 140, 140);">
    <br>
    <h3 class="row col justify-content-center"> SISTEM PERHITUNGAN NILAI </h3>
    <br>
    <div class=" row justify-content-center">
        <div class="col-md-5" >
        <form action="" method="post">
            <div class="col-10 mb-3">
                <label for="nama" class="form-label">NAMA</label>
                <input type="text" name="nama" class="form-control" id="nama">
            </div>
            <div class="col-10 mb-3">
                <label for="mapel" class="form-label">MATA PELAJARAN</label>
                <select name="mapel" class="form-control" id="mapel">
                    <option selected></option>
                    <option value="ILMU PENGETAHUAN ALAM">Ilmu Pengetahuan Alam</option>
                    <option value="ILMU PENGETAHUAN SOSIAL">Ilmu Pengetahuan Sosial</option>
                    <option value="BAHASA INDONESIA">Bahasa Indonesia</option>
                    <option value="BAHASA INGGRIS">Bahasa Inggris</option>
                    <option value="MATEMATIKA">Matematika</option>
                </select>
            </div>
            <div class="col-10 mb-3">
                <label for="uts" class="form-label">Nilai UTS</label>
                <input type="number" name="uts" class="form-control" id="uts">
            </div>
            <div class="col-10 mb-3">
                <label for="uas" class="form-label">Nilai UAS</label>
                <input type="number" name="uas" class="form-control" id="uas">
            </div>
            <div class="col-10 mb-3">
                <label for="tsg" class="form-label">Nilai UAS</label>
                <input type="number" name="tgs" class="form-control" id="tgs">
            </div>
            <button type="submit" name="submit"  class="btn btn-dark justify-content-center">Submit</button>
        </form>  
        </div>

        <div class="col-md-6" >
            <br>
            <br>
        <?php if (isset($_POST['submit'])) : ?> 
        <div class="row justify-content-center">
            <div class="col-8 border rounded-2 border-dark mt-3 p-3" style="background-color: rgb(60, 179, 113)">
              <div class="alert alert-succes" >
                Nama                :<?php echo $nama ?> <br>
                Mata Pelajaran       :<?php echo $matkul ?> <br>
                Nilai UTS           :<?php echo $uts ?> <br>
                Nilai UAS           :<?php echo $uas ?> <br>
                Tugas               :<?php echo $tgs ?> <br>
                Grade               :<?php echo $grade ?> <br>
            </div>
            </div>
        </div>
        <?php endif ; ?>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
  </body>
</html>